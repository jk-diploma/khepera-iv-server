package org.jkondarewicz.kheperaivmanager.mqtt

import java.lang.RuntimeException

class TopicNotValidException(
    private val readableMessage: String
): RuntimeException(readableMessage)