package org.jkondarewicz.kheperaivmanager.mqtt

import java.util.regex.Pattern

data class TopicWildcard private constructor(
    val path: String,
    val qualityOfService: QualityOfService
){

    companion object {

        private const val PATH_REGEX: String = "^(\\/([0-9a-zA-Z\\-_]+|\\+))+(\\/\\#)?\$"

        fun of(path: String, qualityOfService: QualityOfService = QualityOfService.AT_LEAST_ONCE): TopicWildcard =
            if(Pattern.compile(PATH_REGEX).matcher(path).matches())
                TopicWildcard(path, qualityOfService)
            else
                throw TopicNotValidException("Topic is not valid. $path does not match $PATH_REGEX")

    }

}