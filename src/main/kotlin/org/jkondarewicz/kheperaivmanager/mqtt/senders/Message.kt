package org.jkondarewicz.kheperaivmanager.mqtt.senders

abstract class Message {

    abstract fun serialize(): String

}

class EmptyMessage : Message() {
    override fun serialize(): String {
        return "";
    }
}