package org.jkondarewicz.kheperaivmanager.mqtt.senders

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.jkondarewicz.kheperaivmanager.mqtt.QualityOfService
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.springframework.stereotype.Service

@Service
class MqttMessageSender(
    private val mqttClient: IMqttAsyncClient
) {

    fun sendMessage(
        topic: TopicWildcard,
        message: Message,
        qualityOfService: QualityOfService = QualityOfService.AT_LEAST_ONCE
    ) {
        val mqttMessage = MqttMessage(message.serialize().encodeToByteArray())
        mqttMessage.qos = qualityOfService.value
        mqttClient.publish(topic.path, mqttMessage)
    }

}