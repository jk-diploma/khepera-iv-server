package org.jkondarewicz.kheperaivmanager.mqtt.listeners

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.utils.Result
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.annotation.PostConstruct

abstract class MqttTopicListener<T: IncomeMessage> (
    protected val mqttAsyncClient: IMqttAsyncClient,
    protected val mqttTopicListeners: MqttTopicListeners
) {

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @PostConstruct
    fun initialized() {
        listen()
    }

    abstract fun onMessage(message: T, vararg params: String)

    abstract fun topics(): List<TopicWildcard>

    abstract fun deserialize(incomeMessage: String): Result<T>

    private fun listen() {
       topics().forEach {
           mqttTopicListeners.registerListener(it, this)
       }
    }

}