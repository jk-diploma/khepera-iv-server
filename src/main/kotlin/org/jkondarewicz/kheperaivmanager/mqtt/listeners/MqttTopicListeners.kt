package org.jkondarewicz.kheperaivmanager.mqtt.listeners

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.utils.Result
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class MqttTopicListeners(
    private val mqttAsyncClient: IMqttAsyncClient
) {

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    private val listeners: MutableMap<TopicWildcard, MqttTopicListener<IncomeMessage>> = mutableMapOf()

    fun <T: IncomeMessage> registerListener(topic: TopicWildcard, listener: MqttTopicListener<T>) {
        listeners[topic] = listener as MqttTopicListener<IncomeMessage>
        subscribe(topic, listener)
    }

    fun resubscribe() {
        log.info("Resubscribing all registered listeners...")
        listeners.values.forEach {topicListener ->
            topicListener.topics().forEach {
                subscribe(it, topicListener)
            }
        }
        log.info("Resubscribing done!")
    }

    fun unsubscribe(path: String) {
        listeners.keys
            .filter { topic -> topic.path == path }
            .forEach { topicWildcard ->
                if(mqttAsyncClient.isConnected) {
                    mqttAsyncClient.unsubscribe(topicWildcard.path)
                }
                listeners.remove(topicWildcard)
            }
    }

    fun unsubscribe(topic: TopicWildcard) {
        unsubscribe(topic.path)
    }

    private fun <T: IncomeMessage> subscribe(topic: TopicWildcard, listener: MqttTopicListener<T>) {
        if(mqttAsyncClient.isConnected) {
            log.info("Subscribing for topic: ${topic.path} with qos: ${topic.qualityOfService.value}")
            mqttAsyncClient.subscribe(topic.path, topic.qualityOfService.value) { msgTopic, msg ->
                val params = mutableListOf<String>()
                val topicPaths = topic.path.split("/")
                val msgTopicPaths = msgTopic.split("/")
                topicPaths.forEachIndexed { index, topicPath ->
                    if (topicPath == "+" || topicPath == "#") {
                        params.add(msgTopicPaths[index])
                    }
                }
                val message = String(msg.payload)
                log.info("Message arrived at topic: \"$msgTopic\", msg: \"$message\"")
                val deserializeResult = try {
                    listener.deserialize(message)
                } catch (e: Exception) {
                    log.warn("Cannot deserialize message: \"$message\", reason:", e)
                    Result.failure<T>()
                }
                if (deserializeResult.success) {
                    listener.onMessage(deserializeResult.getDataOrException(), *params.toTypedArray())
                } else {
                    log.warn("Cannot deserialize msg: $message")
                }
            }
        }
    }

}