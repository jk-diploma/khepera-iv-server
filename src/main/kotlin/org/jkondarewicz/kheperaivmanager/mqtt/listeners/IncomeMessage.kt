package org.jkondarewicz.kheperaivmanager.mqtt.listeners

import java.time.LocalDateTime

typealias DeviceIdentifier = String

abstract class IncomeMessage(
    open val deviceIdentifier: DeviceIdentifier,
    open val dataTime: LocalDateTime
) {

    companion object {
        const val BASE_DELIMITER = ";"
    }

}