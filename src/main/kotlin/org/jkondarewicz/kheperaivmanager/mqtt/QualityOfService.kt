package org.jkondarewicz.kheperaivmanager.mqtt

enum class QualityOfService {

    AT_MOST_ONCE(0),
    AT_LEAST_ONCE(1),
    EXACTLY_ONE(2);

    val value: Int

    constructor(qualityOfService: Int) {
        this.value = qualityOfService
    }

}