package org.jkondarewicz.kheperaivmanager.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
data class MqttConnectionProperties(

    @Value("\${mqtt.host:localhost}")
    val host: String,

    @Value("\${mqtt.port:1883}")
    val port: Int,

    @Value("\${mqtt.clientIdPrefix:server}")
    val clientIdPrefix: String,

    @Value("\${mqtt:clientId:1234}")
    val clientUuid: String,

    @Value("\${mqtt.connectionTimeoutInSeconds:1}")
    val connectionTimeoutInSeconds: Int
)