package org.jkondarewicz.kheperaivmanager.configuration

import org.eclipse.paho.client.mqttv3.*
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.MqttTopicListeners
import org.jkondarewicz.kheperaivmanager.mqtt.senders.EmptyMessage
import org.jkondarewicz.kheperaivmanager.mqtt.senders.MqttMessageSender
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy

@Configuration
class MqttConnectionConfiguration(
    private val mqttConnectionProperties: MqttConnectionProperties,
    @Lazy
    private val mqttMessageSender: MqttMessageSender,
    @Lazy
    private val mqttTopicListeners: MqttTopicListeners
): MqttCallbackExtended {

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @Bean
    fun connect(): IMqttAsyncClient {
        log.info("Creating connection with MQTT server at: $mqttConnectionProperties")
        val asyncMqttClient: IMqttAsyncClient = MqttAsyncClient(mqttConnectionProperties.serverAddress(), mqttConnectionProperties.clientId())
        asyncMqttClient.setCallback(this)
        asyncMqttClient.connect(prepareMqttConnectionOptions())
        return asyncMqttClient
    }

    override fun connectionLost(cause: Throwable?) {
        log.warn("Connection with MQTT server lost. Reason: ${cause?.message ?: cause}")
    }

    override fun messageArrived(topic: String?, message: MqttMessage?) {}

    override fun deliveryComplete(token: IMqttDeliveryToken?) {}

    override fun connectComplete(reconnect: Boolean, serverURI: String?) {
        log.info("Connection with MQTT at: $serverURI established.")
        mqttTopicListeners.resubscribe()
    }

    private fun prepareMqttConnectionOptions(): MqttConnectOptions =
        MqttConnectOptions().apply {
            this.isAutomaticReconnect = true
            this.isCleanSession = true
            this.connectionTimeout = mqttConnectionProperties.connectionTimeoutInSeconds
        }

    private fun MqttConnectionProperties.serverAddress() =
        "tcp://$host:$port"

    private fun MqttConnectionProperties.clientId(): String =
        "$clientIdPrefix-$clientUuid"

}