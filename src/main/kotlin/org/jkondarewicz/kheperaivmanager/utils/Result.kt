package org.jkondarewicz.kheperaivmanager.utils

class Result<T> private constructor(
    val success: Boolean,
    val data: T? = null
) {

    constructor(data: T) : this(true, data)

    companion object {
        fun <Y> failure() = Result<Y>(false, null)
    }

    fun getDataOrException() =
        data ?: error("xyz")

}