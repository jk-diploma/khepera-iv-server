package org.jkondarewicz.kheperaivmanager.utils

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

class TimeUtils {

    companion object {

        fun fromTimeInMillis(millis: Long): LocalDateTime =
            LocalDateTime.ofEpochSecond(millis / 1000, 0, ZoneId.systemDefault().rules.getOffset(Instant.now()))
    }

}