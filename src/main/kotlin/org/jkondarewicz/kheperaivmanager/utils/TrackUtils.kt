package org.jkondarewicz.kheperaivmanager.utils

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeAction
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

fun List<Track>.assignTracksToRobots(robotIds: List<RobotId>): MutableMap<RobotId, MutableList<Track>> {
    val robotTrackAssignments = mutableMapOf<RobotId, MutableList<Track>>()
    val tracksByDestination = this.sortedBy { it.to.vertexId }.toMutableList()
    var previousTrack: Track? = null
    var robotOffset = 0
    do {
        (tracksByDestination.size - 1 downTo 0).forEach { index ->
            val currentTrack = tracksByDestination[index]
            val sameTrackLeft = tracksByDestination.groupBy { it.to.vertexId }.size == 1
            if (previousTrack == null || currentTrack.to.vertexIndex != previousTrack?.to?.vertexIndex || sameTrackLeft) {
                robotTrackAssignments.addTrack(robotOffset, currentTrack, robotIds)
                robotOffset++
                previousTrack = currentTrack
                tracksByDestination.removeAt(index)
            }
        }
    } while (tracksByDestination.isNotEmpty())
    return robotTrackAssignments
}

fun MutableMap<RobotId, MutableList<Track>>.toMessage(robotId: RobotId) =
    if (!this.containsKey(robotId) || this[robotId].isNullOrEmpty()) null
    else
        this[robotId]!!.map { track ->
            track.path.map { edge ->
                "${edge.crossId}:${edge.edgeAction.toChar()}:${edge.to.vertexType.toVertexType()}"
            }
        }.flatten().joinToString(separator = "-")

private fun VertexType.toVertexType(): String =
    when (this) {
        VertexType.NONE -> "N"
        VertexType.COLLECT_POINT -> "C"
        VertexType.START -> "S"
        VertexType.END -> "E"
    }

private fun EdgeAction.toChar() =
    when (this) {
        EdgeAction.TURN_LEFT -> "L"
        EdgeAction.TURN_RIGHT -> "R"
        EdgeAction.FORWARD -> "F"
    }


private fun MutableMap<RobotId, MutableList<Track>>.addTrack(robotOffset: Int, track: Track, robotIds: List<RobotId>) {
    val key = robotIds[robotOffset % robotIds.size]
    if (!this.containsKey(key)) {
        this[key] = mutableListOf()
    }
    this[key]?.add(track)
}
