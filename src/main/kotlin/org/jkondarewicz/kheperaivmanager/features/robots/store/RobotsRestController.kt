package org.jkondarewicz.kheperaivmanager.features.robots.store

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController("/robots")
class RobotsRestController(
    private val robotsStoreService: RobotsStoreService
) {

    @GetMapping
    fun robots() =
        robotsStoreService.robotsInfo()

}