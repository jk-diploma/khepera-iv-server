package org.jkondarewicz.kheperaivmanager.features.robots.heartbeat

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.jkondarewicz.kheperaivmanager.features.robots.RobotsFacade
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.IncomeMessage
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.MqttTopicListener
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.MqttTopicListeners
import org.jkondarewicz.kheperaivmanager.utils.Result
import org.jkondarewicz.kheperaivmanager.utils.TimeUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class RobotHeartbeatListener(
    private val robotsFacade: RobotsFacade,
    mqttAsyncClient: IMqttAsyncClient,
    mqttTopicListeners: MqttTopicListeners
) : MqttTopicListener<RobotHearbeatMessage>(mqttAsyncClient, mqttTopicListeners) {

    private val log: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        const val ROBOTS_BASIC_INFO_PATH = "/robots/+/heartbeat"
    }

    override fun onMessage(message: RobotHearbeatMessage, vararg params: String) {
        log.debug("Processing message: $message")
        robotsFacade.heartbeat(
            robotId = message.deviceIdentifier,
            batteryPercentage = message.batteryPercentage,
            heartbeatTime = message.dataTime
        )
    }

    override fun topics(): List<TopicWildcard> =
        listOf(
            TopicWildcard.of(ROBOTS_BASIC_INFO_PATH)
        )

    override fun deserialize(incomeMessage: String): Result<RobotHearbeatMessage> =
        incomeMessage.split(IncomeMessage.BASE_DELIMITER).let { parts ->
            Result(
                RobotHearbeatMessage(
                    parts[0],
                    TimeUtils.fromTimeInMillis(parts[1].toLong()),
                    parts[2].toInt()
                )
            )
        }
}