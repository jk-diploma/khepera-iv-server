package org.jkondarewicz.kheperaivmanager.features.robots

import org.jkondarewicz.kheperaivmanager.features.robots.store.RobotsStoreFacade
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class RobotsFacade(
    private val robotsStoreFacade: RobotsStoreFacade
) {

    fun robotsInfo() =
        robotsStoreFacade.robotsInfo()

    fun heartbeat(robotId: RobotId, batteryPercentage: Int, heartbeatTime: LocalDateTime) =
        robotsStoreFacade.heartbeat(robotId, batteryPercentage, heartbeatTime)

    fun assignRobotToTrack(robotId: RobotId, trackId: TrackId) {
        robotsStoreFacade.assignTrack(robotId, trackId)
    }

    fun unassignRobotToTrack(robotId: RobotId, trackId: TrackId) {
        robotsStoreFacade.unassignTrack(robotId, trackId)
    }

}