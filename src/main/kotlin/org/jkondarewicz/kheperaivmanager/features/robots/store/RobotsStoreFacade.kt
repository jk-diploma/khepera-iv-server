package org.jkondarewicz.kheperaivmanager.features.robots.store

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class RobotsStoreFacade(
    private val robotsStoreService: RobotsStoreService
) {

    fun robotsInfo() =
        robotsStoreService.robotsInfo()

    fun heartbeat(robotId: RobotId, batteryPercentage: Int, heartbeatTime: LocalDateTime) {
        robotsStoreService.heartbeat(robotId, batteryPercentage, heartbeatTime)
    }

    fun assignTrack(robotId: RobotId, trackId: TrackId) {
        robotsStoreService.assignTrack(robotId, trackId)
    }

    fun unassignTrack(robotId: RobotId, trackId: TrackId) {
        robotsStoreService.unassignTrack(robotId, trackId)
    }

}