package org.jkondarewicz.kheperaivmanager.features.robots.heartbeat

import org.jkondarewicz.kheperaivmanager.mqtt.listeners.DeviceIdentifier
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.IncomeMessage
import java.time.LocalDateTime

data class RobotHearbeatMessage(
    override val deviceIdentifier: DeviceIdentifier,
    override val dataTime: LocalDateTime,
    val batteryPercentage: Int
) : IncomeMessage(deviceIdentifier, dataTime)