package org.jkondarewicz.kheperaivmanager.features.robots.store

import org.jkondarewicz.kheperaivmanager.features.robots.store.dto.RobotInfo
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotEntity
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.TrackRepository
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
class RobotsStoreService(
    private val robotsRepository: RobotsRepository,
    private val trackRepository: TrackRepository
) {

    fun robotsInfo() =
        robotsRepository
            .findAll()
            .map { RobotInfo.ofRobotEntity(it) }
            .sortedWith(compareBy({ it.track?.trackId }, { it.track?.whichInOrder }))

    fun heartbeat(robotId: RobotId, batteryPercentage: Int, heartbeatTime: LocalDateTime): RobotInfo {
        val robot = robotsRepository.findByIdOrNull(robotId)
            ?: RobotEntity.of(robotId, batteryPercentage, heartbeatTime)
        return RobotInfo.ofRobotEntity(robotsRepository.save(robot.heartbeat(batteryPercentage, heartbeatTime)))
    }

    @Transactional
    fun assignTrack(robotId: RobotId, trackId: TrackId) {
        val track = trackRepository.findByIdOrNull(trackId)
            ?: error("Track with id $trackId does not exist")
        val robot = robotsRepository.findByIdOrNull(robotId) ?: error("Robot with id $robotId does not exist")
        if (robot.assignedTrack?.trackId != trackId) {
            robot.whichInOrder = reorderRobotsQueue(trackId)
            robot.assignedTrack = track
        }
    }

    @Transactional
    fun unassignTrack(robotId: RobotId, trackId: TrackId) {
        val robot = robotsRepository.findByIdOrNull(robotId) ?: error("Robot with id $robotId does not exist")
        if (robot.assignedTrack?.trackId != null) {
            robot.assignedTrack = null
            robot.whichInOrder = null
            reorderRobotsQueue(trackId)
        }
    }

    private fun reorderRobotsQueue(trackId: TrackId): Int {
        var whichInOrder = 0
        robotsRepository
            .findAllByAssignedTrack_TrackId(trackId)
            .sortedBy { it.whichInOrder }
            .forEach { it.whichInOrder = ++whichInOrder }
        return ++whichInOrder
    }

}