package org.jkondarewicz.kheperaivmanager.features.robots.store.dto

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotEntity
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import java.time.LocalDateTime

data class RobotInfo(
    val robotId: RobotId,
    val batteryPercentage: Int,
    val lastHeartbeat: LocalDateTime,
    val track: RobotTrack? = null
) {

    companion object {

        fun ofRobotEntity(robotEntity: RobotEntity) =
            RobotInfo(
                robotId = robotEntity.robotId,
                batteryPercentage = robotEntity.batteryPercentage,
                lastHeartbeat = robotEntity.heartbeat,
                track = robotEntity.assignedTrack?.let { trackEntity ->
                    RobotTrack(
                        trackId = trackEntity.getIdOrException(),
                        name = trackEntity.name,
                        whichInOrder = robotEntity.getWhichInOrderOrException()
                    )
                }
            )

    }

}

data class RobotTrack(
    val trackId: TrackId,
    val name: String,
    val whichInOrder: Int
)