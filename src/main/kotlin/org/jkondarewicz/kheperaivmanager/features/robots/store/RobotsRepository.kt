package org.jkondarewicz.kheperaivmanager.features.robots.store

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import org.springframework.data.jpa.repository.JpaRepository

interface RobotsRepository : JpaRepository<RobotEntity, String> {

    fun findAllByAssignedTrack_TrackId(trackId: TrackId): List<RobotEntity>

}