package org.jkondarewicz.kheperaivmanager.features.robots.store.entities

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackEntity
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table

typealias RobotId = String

@Entity
@Table(name = "robots")
class RobotEntity(
    @Id
    val robotId: RobotId,
    var batteryPercentage: Int,
    var heartbeat: LocalDateTime,
    @ManyToOne
    var assignedTrack: TrackEntity? = null,
    var whichInOrder: Int? = null
) {

    companion object {

        fun of(robotId: RobotId, batteryPercentage: Int, heartbeat: LocalDateTime) =
            RobotEntity(robotId, batteryPercentage, heartbeat)

    }

    fun heartbeat(batteryPercentage: Int, time: LocalDateTime): RobotEntity =
        RobotEntity(this.robotId, batteryPercentage, time, this.assignedTrack, this.whichInOrder)

    fun getWhichInOrderOrException() =
        this.whichInOrder ?: error("Which in order is not set!")

}