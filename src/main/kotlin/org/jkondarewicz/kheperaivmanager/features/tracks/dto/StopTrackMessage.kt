package org.jkondarewicz.kheperaivmanager.features.tracks.dto

import org.jkondarewicz.kheperaivmanager.mqtt.senders.Message

data class StopTrackMessage(
    private val robotId: String
) : Message() {

    override fun serialize(): String =
        "$robotId"

}