package org.jkondarewicz.kheperaivmanager.features.tracks

import org.jkondarewicz.kheperaivmanager.features.robots.RobotsFacade
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class TracksViewController(
    private val robotsFacade: RobotsFacade,
    private val tracksService: TracksService
) {

    @GetMapping("/tracks")
    fun listOfAvailableTracks(model: Model): String {
        model.addAttribute("tracks", tracksService.findAllTracks())
        return "tracks"
    }

    @GetMapping("/tracks/{trackId}")
    fun chosenTrack(@PathVariable trackId: Int, model: Model): String {
        val robots = robotsFacade.robotsInfo()
        model.addAttribute("track", tracksService.findTrack(trackId))
        model.addAttribute("allRobots", robots.filter { it.track?.trackId != trackId })
        model.addAttribute("trackRobots", robots.filter { it.track?.trackId == trackId })
        return "chosen_track"
    }

    @PostMapping("/tracks/{trackId}/robots/{robotId}/assign")
    fun assignRobotToTrack(
        @PathVariable trackId: TrackId,
        @PathVariable robotId: RobotId,
        model: Model): ModelAndView {
        robotsFacade.assignRobotToTrack(robotId, trackId)
        return ModelAndView("redirect:/tracks/${trackId}")
    }

    @PostMapping("/tracks/{trackId}/robots/{robotId}/unassign")
    fun unassignRobotToTrack(
        @PathVariable trackId: TrackId,
        @PathVariable robotId: RobotId,
        model: Model): ModelAndView {
        robotsFacade.unassignRobotToTrack(robotId, trackId)
        return ModelAndView("redirect:/tracks/${trackId}")
    }

    @PostMapping("/tracks/{trackId}/start")
    fun startTrack(@PathVariable trackId: TrackId): ModelAndView {
        tracksService.startTrack(trackId)
        return ModelAndView("redirect:/tracks/${trackId}")
    }

    @PostMapping("/tracks/{trackId}/stop")
    fun stopTrack(@PathVariable trackId: TrackId): ModelAndView {
        tracksService.stopTrack(trackId)
        return ModelAndView("redirect:/tracks/${trackId}")
    }


}