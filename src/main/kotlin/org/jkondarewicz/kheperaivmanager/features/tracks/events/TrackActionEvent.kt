package org.jkondarewicz.kheperaivmanager.features.tracks.events

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.springframework.context.ApplicationEvent

data class TrackActionEvent(
    val sourcePublisher: Any,
    val robotIds: List<RobotId>,
    val action: TrackAction,
    val trackMessage: MutableMap<RobotId, MutableList<Track>> = mutableMapOf()
) : ApplicationEvent(sourcePublisher)

enum class TrackAction {
    START,
    STOP
}