package org.jkondarewicz.kheperaivmanager.features.tracks.dto

typealias VertexIndex = Int
typealias ToVertexIndex = Int
typealias ToVertexWeight = Int

data class Graph(
    val vertexes: List<Vertex>
) {

    fun toAdjacencyMatrix(): List<List<ToVertexWeight>> {
        val vertexIndexesMap = mutableMapOf<VertexIndex, MutableMap<ToVertexIndex, ToVertexWeight>>()
        this.vertexes.forEach { vertex ->
            val nextVertexes = mutableMapOf<ToVertexIndex, ToVertexWeight>()
            vertex.nextVertexes.forEach { nextVertex ->
                nextVertexes[nextVertex.vertexIndex] = nextVertex.weight
            }
            vertexIndexesMap[vertex.vertexIndex] = nextVertexes
        }
        val vertexesSize = vertexIndexesMap.size
        val adjacencyMatrix = mutableListOf<MutableList<ToVertexWeight>>()
        (0 until vertexesSize).forEach { key ->
            val nextVertexes = vertexIndexesMap[key]!!
            val nextVertexesWeights = mutableListOf<ToVertexWeight>()
            (0 until vertexesSize).forEach { nextVertexKey ->
                nextVertexesWeights.add(nextVertexes.getOrDefault(nextVertexKey, 0))
            }
            adjacencyMatrix.add(nextVertexesWeights)
        }
        return adjacencyMatrix
    }

}