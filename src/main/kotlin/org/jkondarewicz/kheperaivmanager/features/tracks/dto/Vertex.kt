package org.jkondarewicz.kheperaivmanager.features.tracks.dto

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeAction
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

data class Vertex(
    val vertexId: Int,
    val vertexType: VertexType,
    val vertexIndex: Int,
    val nextVertexes: List<NextVertex>,
    val conflictPossible: Boolean,
    val howMany: Int
)

data class NextVertex(
    val edgeId: Int,
    val edgeAction: EdgeAction,
    val weight: Int,
    val vertexIndex: Int,
    val crossId: Int
)