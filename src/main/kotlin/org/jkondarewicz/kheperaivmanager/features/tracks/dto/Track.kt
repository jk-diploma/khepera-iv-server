package org.jkondarewicz.kheperaivmanager.features.tracks.dto

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeAction
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

data class Track(
    val from: TrackVertex,
    val to: TrackVertex,
    val summaryWeight: Int,
    val path: List<TrackEdge>,
    val by: TrackVertex? = null
) {

    companion object {

        fun mergeTracks(first: Track, second: Track) = Track(
            from = first.from,
            by = second.from,
            to = second.to,
            summaryWeight = first.summaryWeight + second.summaryWeight,
            path = listOf(first.path, second.path).flatten()
        )

    }

}

data class TrackVertex(
    val vertexId: Int,
    val vertexIndex: VertexIndex,
    val vertexType: VertexType,
    val conflictPossible: Boolean
)

data class TrackEdge(
    val edgeId: Int,
    val crossId: Int,
    val edgeAction: EdgeAction,
    val weight: Int,
    val from: TrackVertex,
    val to: TrackVertex
)
