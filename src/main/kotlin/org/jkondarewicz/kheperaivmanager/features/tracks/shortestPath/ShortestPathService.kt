package org.jkondarewicz.kheperaivmanager.features.tracks.shortestPath

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Graph
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.VertexIndex

interface ShortestPathService {

    fun generateShortestPath(graph: Graph, sourceIndex: VertexIndex, targetIndex: VertexIndex): Track

    fun generateShortestPaths(graph: Graph, sourceIndex: VertexIndex): List<Track>

}