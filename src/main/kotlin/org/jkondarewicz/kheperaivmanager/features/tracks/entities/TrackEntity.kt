package org.jkondarewicz.kheperaivmanager.features.tracks.entities

import org.hibernate.annotations.Type
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Graph
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.NextVertex
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Vertex
import javax.persistence.*

typealias TrackId = Int

@Entity
@Table(name = "tracks", uniqueConstraints = [UniqueConstraint(name = "unique_track_name", columnNames = ["trackId", "name"])])
class TrackEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var trackId: TrackId?,
    val name: String,
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    var previewImage: ByteArray?,
    var started: Boolean = false,
    @OneToMany(orphanRemoval = true, cascade = [CascadeType.ALL], mappedBy = "track")
    val vertexes: MutableSet<VertexEntity> = HashSet(),
    @OneToMany(mappedBy = "assignedTrack")
    val assignedRobots: List<RobotEntity> = listOf()
) {

    fun getIdOrException(): Int = trackId ?: error("Track does not contain id")

    fun addVertex(vertexEntity: VertexEntity) {
        vertexes.add(vertexEntity)
    }

    fun toGraph(): Graph {
        val vertexes = this.vertexes.map { vertexEntity ->
            Vertex(
                vertexId = vertexEntity.getIdOrException(),
                vertexType = vertexEntity.type,
                vertexIndex = vertexEntity.vertexIndex,
                nextVertexes = vertexEntity.outputEdges.map { edgeEntity ->
                    NextVertex(
                        edgeId = edgeEntity.getEdgeIdOrException(),
                        edgeAction = edgeEntity.action,
                        weight = edgeEntity.weight,
                        vertexIndex = edgeEntity.to.vertexIndex,
                        crossId = edgeEntity.crossQueue
                    )
                },
                conflictPossible = vertexEntity.inputEdges.size > 1,
                howMany = vertexEntity.collectPointQuantity
            )
        }
        return Graph(vertexes = vertexes)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TrackEntity

        if (trackId != other.trackId) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = trackId ?: 0
        result = 31 * result + name.hashCode()
        return result
    }

}