package org.jkondarewicz.kheperaivmanager.features.tracks.dto

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

data class StorehouseTrack(
    val trackId: Int,
    val name: String,
    val vertexesQuantity: Int,
    val collectPointsQuantity: Int,
    val assignedRobotsQuantity: Int,
    val started: Boolean
) {

    companion object {

        fun fromTrackEntity(track: TrackEntity): StorehouseTrack =
            StorehouseTrack(
                trackId = track.getIdOrException(),
                name = track.name,
                vertexesQuantity = track.vertexes.size,
                collectPointsQuantity = track.vertexes.filter { vertex -> vertex.type == VertexType.COLLECT_POINT }.size,
                assignedRobotsQuantity = track.assignedRobots.size,
                started = track.started
            )

    }

}