package org.jkondarewicz.kheperaivmanager.features.tracks

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TrackRepository : JpaRepository<TrackEntity, Int>