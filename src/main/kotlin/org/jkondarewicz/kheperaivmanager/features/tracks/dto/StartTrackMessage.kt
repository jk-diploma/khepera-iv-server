package org.jkondarewicz.kheperaivmanager.features.tracks.dto

import org.jkondarewicz.kheperaivmanager.mqtt.senders.Message

data class StartTrackMessage(
    val robotId: String,
    val crossingActions: String
) : Message() {

    override fun serialize(): String =
        "$robotId;$crossingActions"

}