package org.jkondarewicz.kheperaivmanager.features.tracks.shortestPath

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.*
import org.springframework.stereotype.Service


@Service
class DijkstraShortestPathService : ShortestPathService {

    override fun generateShortestPath(graph: Graph, sourceIndex: VertexIndex, targetIndex: VertexIndex): Track =
        this.generateShortestPaths(graph, sourceIndex).first { track -> track.to.vertexIndex == targetIndex }

    override fun generateShortestPaths(graph: Graph, sourceIndex: Int): List<Track> {
        val adjacencyMatrix = graph.toAdjacencyMatrix()
        val verticesSize = adjacencyMatrix[0].size
        val dataHolder = DijkstraDataHolder(sourceIndex, verticesSize)
        return dataHolder.generateTracks(graph)
    }

}

private data class DijkstraDataHolder private constructor(
    val shortestDistances: MutableList<Int> = mutableListOf(),
    val verticesIncludedInShortestDistance: MutableList<Boolean> = mutableListOf(),
    val parents: MutableList<Int> = mutableListOf(),
    private val verticesSize: Int,
    private val sourceIndex: Int
) {

    private val NO_PARENT = -1

    constructor(sourceIndex: Int, verticesQuantity: Int) : this(verticesSize = verticesQuantity, sourceIndex = sourceIndex) {
        initializeDataWithDefaultValues()
        shortestDistances[sourceIndex] = 0
    }

    fun generateTracks(graph: Graph): MutableList<Track> {
        calculateShortestPaths(graph.toAdjacencyMatrix())
        val tracks = mutableListOf<Track>()
        (0 until shortestDistances.size).forEach { vertexIndex ->
            if (vertexIndex != sourceIndex) {
                val path = parents.generatePath(vertexIndex)
                tracks.add(
                    Track(
                        from = sourceIndex.toTrackVertex(graph),
                        to = vertexIndex.toTrackVertex(graph),
                        summaryWeight = shortestDistances[vertexIndex],
                        path = path.toTrackPath(graph)))
            }
        }
        return tracks
    }

    private fun calculateShortestPaths(adjacencyMatrix: List<List<ToVertexWeight>>) {
        (1 until verticesSize).forEach { _ ->
            var nearestVertex = -1
            var shortestDistance = Int.MAX_VALUE
            (0 until verticesSize).forEach { vertexIndex ->
                if (!verticesIncludedInShortestDistance[vertexIndex] && shortestDistances[vertexIndex] < shortestDistance) {
                    nearestVertex = vertexIndex
                    shortestDistance = shortestDistances[vertexIndex]
                }
            }
            verticesIncludedInShortestDistance[nearestVertex] = true
            (0 until verticesSize).forEach { vertexIndex ->
                val edgeDistance = adjacencyMatrix[nearestVertex][vertexIndex]
                if (edgeDistance > 0 && ((shortestDistance + edgeDistance) < shortestDistances[vertexIndex])) {
                    parents[vertexIndex] = nearestVertex
                    shortestDistances[vertexIndex] = shortestDistance + edgeDistance
                }
            }
        }
    }

    private fun initializeDataWithDefaultValues() {
        (0 until verticesSize).forEach { vertexIndex ->
            shortestDistances.add(vertexIndex, Int.MAX_VALUE)
            verticesIncludedInShortestDistance.add(vertexIndex, false)
            parents.add(vertexIndex, NO_PARENT)
        }
    }

    private fun VertexIndex.toTrackVertex(graph: Graph) =
        graph.vertexes.first { vertex -> vertex.vertexIndex == this }.let { vertex -> vertex.toTrackVertex() }

    private fun Vertex.toTrackVertex() =
        TrackVertex(
            vertexId = vertexId,
            vertexIndex = vertexIndex,
            vertexType = vertexType,
            conflictPossible = conflictPossible
        )

    private fun List<VertexIndex>.toTrackPath(graph: Graph) =
        (0 until (this.size - 1)).map { vertexIndex ->
            val currentVertex = graph.vertexes.first { vertex -> vertex.vertexIndex == this[vertexIndex] }
            val nextVertex = graph.vertexes.first { vertex -> vertex.vertexIndex == this[vertexIndex + 1] }
            val edge = currentVertex.nextVertexes.first { edge -> edge.vertexIndex == nextVertex.vertexIndex }
            TrackEdge(
                edgeId = edge.edgeId,
                edgeAction = edge.edgeAction,
                to = nextVertex.toTrackVertex(),
                from = currentVertex.toTrackVertex(),
                weight = edge.weight,
                crossId = edge.crossId
            )
        }

    private fun List<Int>.generatePath(vertexIndex: VertexIndex): List<VertexIndex> {
        val path = mutableListOf<VertexIndex>()
        path.add(vertexIndex)
        var currentVertex: VertexIndex = vertexIndex
        while (this[currentVertex] != NO_PARENT) {
            path.add(this[currentVertex])
            currentVertex = this[currentVertex]
        }
        return path.reversed()
    }

}


