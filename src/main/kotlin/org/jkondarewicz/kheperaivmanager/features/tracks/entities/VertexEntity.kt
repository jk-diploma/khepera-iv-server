package org.jkondarewicz.kheperaivmanager.features.tracks.entities

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.VertexIndex
import javax.persistence.*

typealias VertexId = Int

@Entity
@Table(name = "vertexes", uniqueConstraints = [UniqueConstraint(name = "unique_vertex_index", columnNames = ["track_track_id", "vertexIndex"])])
class VertexEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var vertexId: VertexId? = null,
    @Enumerated(EnumType.STRING)
    val type: VertexType,
    @Column
    val vertexIndex: VertexIndex,
    @OneToMany(mappedBy = "from", orphanRemoval = true, cascade = [CascadeType.PERSIST])
    val outputEdges: MutableSet<EdgeEntity> = HashSet(),
    @OneToMany(mappedBy = "to", orphanRemoval = true, cascade = [CascadeType.PERSIST])
    val inputEdges: MutableSet<EdgeEntity> = HashSet(),
    @ManyToOne
    var track: TrackEntity? = null,
    var collectPointQuantity: Int = 0
) {

    fun getIdOrException() = vertexId ?: error("Vertex id cannot be null")

    fun addOutputEdge(outputEdge: EdgeEntity) {
        this.outputEdges.add(outputEdge)
    }

    fun addInputEdge(inputEdge: EdgeEntity) {
        this.inputEdges.add(inputEdge)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VertexEntity

        if (vertexId != other.vertexId) return false
        if (type != other.type) return false
        if (vertexIndex != other.vertexIndex) return false

        return true
    }

    override fun hashCode(): Int {
        var result = vertexId ?: 0
        result = 31 * result + type.hashCode()
        result = 31 * result + vertexIndex
        return result
    }

}

enum class VertexType {
    START,
    COLLECT_POINT,
    END,
    NONE
}
