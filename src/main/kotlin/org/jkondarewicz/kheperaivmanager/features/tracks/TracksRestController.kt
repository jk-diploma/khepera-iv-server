package org.jkondarewicz.kheperaivmanager.features.tracks

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class TracksRestController(
    private val tracksService: TracksService
) {

    @GetMapping("/tracks/{trackId}/preview-image", produces = [MediaType.IMAGE_JPEG_VALUE])
    fun previewImageOfTracks(@PathVariable trackId: Int) =
        tracksService.previewImageOfTracks(trackId)

}