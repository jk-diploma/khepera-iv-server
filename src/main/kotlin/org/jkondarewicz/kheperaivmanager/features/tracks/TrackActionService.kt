package org.jkondarewicz.kheperaivmanager.features.tracks

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.StartTrackMessage
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.StopTrackMessage
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.jkondarewicz.kheperaivmanager.features.tracks.events.TrackAction.START
import org.jkondarewicz.kheperaivmanager.features.tracks.events.TrackAction.STOP
import org.jkondarewicz.kheperaivmanager.features.tracks.events.TrackActionEvent
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.mqtt.senders.MqttMessageSender
import org.jkondarewicz.kheperaivmanager.utils.toMessage
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

@Service
class TrackActionService(
    private val mqttMessageSender: MqttMessageSender
) {

    private val log = LoggerFactory.getLogger(this::class.java)

    @Async
    @EventListener
    fun onTrackActionEvent(trackActionEvent: TrackActionEvent) {
        log.debug("Received event: $trackActionEvent")
        when (trackActionEvent.action) {
            START -> sendStartTrackMessages(trackActionEvent.robotIds, trackActionEvent.trackMessage)
            STOP -> sendStopTrackMessages(trackActionEvent.robotIds)
        }
    }

    private fun sendStopTrackMessages(robotIds: List<RobotId>) {
        robotIds.forEach { robotId ->
            mqttMessageSender.sendMessage(TopicWildcard.of("/robots/$robotId/stop-track"), StopTrackMessage(robotId))
        }
    }

    private fun sendStartTrackMessages(robotIds: List<RobotId>, trackMessage: MutableMap<RobotId, MutableList<Track>>) {
        robotIds.forEach { robotId ->
            trackMessage.toMessage(robotId)?.let { message ->
                log.info("Message {}", message)
                mqttMessageSender.sendMessage(TopicWildcard.of("/robots/$robotId/start-track"), StartTrackMessage(robotId, message))
            }
        }
    }

}