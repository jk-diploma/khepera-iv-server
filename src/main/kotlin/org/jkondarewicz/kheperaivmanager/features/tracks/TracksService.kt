package org.jkondarewicz.kheperaivmanager.features.tracks

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Graph
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.StorehouseTrack
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Vertex
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType
import org.jkondarewicz.kheperaivmanager.features.tracks.events.TrackAction
import org.jkondarewicz.kheperaivmanager.features.tracks.events.TrackActionEvent
import org.jkondarewicz.kheperaivmanager.features.tracks.shortestPath.ShortestPathService
import org.jkondarewicz.kheperaivmanager.utils.assignTracksToRobots
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class TracksService(
    private val trackRepository: TrackRepository,
    private val shortestPathService: ShortestPathService,
    private val applicationEventPublisher: ApplicationEventPublisher
) {

    companion object {

        private val FALLBACK_IMAGE
            get() = Thread.currentThread().contextClassLoader.getResourceAsStream("fallback_image.jpg")

    }

    @Transactional
    fun findAllTracks(): List<StorehouseTrack> =
        trackRepository.findAll().map { StorehouseTrack.fromTrackEntity(it) }

    fun findTrack(trackId: Int): StorehouseTrack =
        StorehouseTrack.fromTrackEntity(getTrackOrException(trackId))

    fun previewImageOfTracks(trackId: Int): ByteArray =
        trackRepository.findByIdOrNull(trackId)?.previewImage ?: FALLBACK_IMAGE.readAllBytes()

    @Transactional
    fun findShortestPathsToCollectPointAndBack(trackId: Int): List<Track> {
        val track = getTrackOrException(trackId)
        val graph = track.toGraph()
        val startingPoint = graph.vertexes.firstOrNull { vertex -> vertex.vertexType == VertexType.START }
            ?: error("Track does not contain starting point")
        val possiblePaths = shortestPathService.generateShortestPaths(graph, startingPoint.vertexIndex)
        val collectPoints = graph.vertexes.filter { vertex -> vertex.vertexType == VertexType.COLLECT_POINT }
        if (collectPoints.isEmpty()) error("Track does not have defined collect points")
        return generateCollectPointTracks(graph, possiblePaths, collectPoints)
    }

    @Transactional
    fun startTrack(trackId: TrackId) {
        val track = getTrackOrException(trackId)
        val robotIds = track.assignedRobots.sortedBy { it.whichInOrder }.map { it.robotId }
        track.started = true
        val trackActionEvent = TrackActionEvent(
            sourcePublisher = this,
            action = TrackAction.START,
            robotIds = robotIds,
            trackMessage = findShortestPathsToCollectPointAndBack(trackId).assignTracksToRobots(robotIds)
        )
        applicationEventPublisher.publishEvent(trackActionEvent)
    }

    @Transactional
    fun stopTrack(trackId: TrackId) {
        val track = getTrackOrException(trackId)
        track.started = false
        applicationEventPublisher.publishEvent(TrackActionEvent(this, track.assignedRobots.sortedBy { it.whichInOrder }.map { it.robotId }, TrackAction.STOP))
    }

    private fun generateCollectPointTracks(graph: Graph, startPointTracks: List<Track>, collectPoints: List<Vertex>) =
        collectPoints.map { collectPoint ->
            val fromStartPointToCollectPointTrack = startPointTracks.first { track -> track.to.vertexIndex == collectPoint.vertexIndex }
            val fromCollectPointToStartPointTrack = shortestPathService.generateShortestPath(graph, collectPoint.vertexIndex, fromStartPointToCollectPointTrack.from.vertexIndex)
            (0 until collectPoint.howMany).map { Track.mergeTracks(fromStartPointToCollectPointTrack, fromCollectPointToStartPointTrack) }
        }.flatten()

    private fun getTrackOrException(trackId: TrackId) =
        trackRepository.findByIdOrNull(trackId) ?: error("xyz")


}
