package org.jkondarewicz.kheperaivmanager.features.tracks.entities

import javax.persistence.*

@Entity
@Table(name = "edges")
class EdgeEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var edgeId: Int? = null,
    val weight: Int,
    @Enumerated(EnumType.STRING)
    val action: EdgeAction,
    @ManyToOne(cascade = [CascadeType.PERSIST])
    val from: VertexEntity,
    @ManyToOne(cascade = [CascadeType.PERSIST])
    val to: VertexEntity,
    val crossQueue: Int
) {
    fun getEdgeIdOrException() = edgeId ?: error("Edge id cannot be null")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EdgeEntity

        if (edgeId != other.edgeId) return false
        if (weight != other.weight) return false
        if (action != other.action) return false
        if (crossQueue != other.crossQueue) return false
        return true
    }

    override fun hashCode(): Int {
        var result = edgeId ?: 0
        result = 31 * result + weight
        result = 31 * result + action.hashCode()
        return result
    }
}

enum class EdgeAction {
    TURN_LEFT,
    TURN_RIGHT,
    FORWARD
}