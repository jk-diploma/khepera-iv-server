package org.jkondarewicz.kheperaivmanager.features.trackConflicts

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.entities.ConflictCrossEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexId
import org.springframework.data.jpa.repository.JpaRepository

interface ConflictCrossRepository : JpaRepository<ConflictCrossEntity, VertexId> {

    fun findAllByCrossQueueAndRobotId(crossId: CrossId, robotId: RobotId): List<ConflictCrossEntity>

}