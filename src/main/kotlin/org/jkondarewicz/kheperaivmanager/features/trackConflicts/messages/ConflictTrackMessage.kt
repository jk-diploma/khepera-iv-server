package org.jkondarewicz.kheperaivmanager.features.trackConflicts.messages

import org.jkondarewicz.kheperaivmanager.features.trackConflicts.CrossId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexId
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.DeviceIdentifier
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.IncomeMessage
import java.time.LocalDateTime

class ConflictTrackMessage(
    override val deviceIdentifier: DeviceIdentifier,
    override val dataTime: LocalDateTime,
    val crossId: CrossId
) : IncomeMessage(deviceIdentifier, dataTime) {

    companion object {

        val DELIMITER = ";"
        val VALUES_QUANTITY = 3

    }

}