package org.jkondarewicz.kheperaivmanager.features.trackConflicts.dto

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.CrossId
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.entities.ConflictCrossEntity
import java.time.LocalDateTime

data class ConflictVertex(
    val crossId: CrossId,
    val robotId: RobotId,
    val startConflictTime: LocalDateTime,
    var sendStartMessageTime: LocalDateTime? = null,
    var sendStartMessage: Boolean = false
) {
    fun toEntity() = ConflictCrossEntity(
        crossQueue = crossId,
        robotId = robotId,
        sendStartMessageTime = sendStartMessageTime,
        startConflictTime = startConflictTime
    )
}

fun ConflictCrossEntity.toConflictVertex() =
    ConflictVertex(
        crossId = this.crossQueue,
        robotId = this.robotId,
        sendStartMessageTime = this.sendStartMessageTime,
        startConflictTime = this.startConflictTime,
        sendStartMessage = this.sendStartMessageTime != null
    )