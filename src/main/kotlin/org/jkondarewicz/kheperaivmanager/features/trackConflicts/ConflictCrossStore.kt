package org.jkondarewicz.kheperaivmanager.features.trackConflicts

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.dto.toConflictVertex
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.entities.ConflictCrossEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexId
import org.springframework.stereotype.Service

@Service
class ConflictCrossStore(
    private val conflictCrossRepository: ConflictCrossRepository
) {

    fun getAll() =
        conflictCrossRepository.findAll().map { it.toConflictVertex() }

    fun save(conflictCrossEntity: ConflictCrossEntity) {
        conflictCrossRepository.save(conflictCrossEntity)
    }

    fun delete(robotId: RobotId, crossId: CrossId) {
        conflictCrossRepository.findAllByCrossQueueAndRobotId(crossId, robotId).forEach {
            conflictCrossRepository.deleteById(it.id)
        }
    }

}