package org.jkondarewicz.kheperaivmanager.features.trackConflicts.entities

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.CrossId
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexId
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "conflicts_crossings")
class ConflictCrossEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,
    val crossQueue: CrossId,
    val robotId: RobotId,
    val startConflictTime: LocalDateTime,
    val sendStartMessageTime: LocalDateTime? = null
)