package org.jkondarewicz.kheperaivmanager.features.trackConflicts

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class ConflictCrossProperties(
    @Value("\${robots.minimumSecondsToResendStartMessage}")
    val minimumSecondsToResendStartMessage: Long,
    @Value("\${robots.minimumDelayInSecondsBetweenStartConflictToDeleteRobotFromList}")
    val minimumDelayInSecondsBetweenStartConflictToDeleteRobotFromList: Long
)