package org.jkondarewicz.kheperaivmanager.features.trackConflicts

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.messages.ConflictTrackMessage
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.MqttTopicListener
import org.jkondarewicz.kheperaivmanager.mqtt.listeners.MqttTopicListeners
import org.jkondarewicz.kheperaivmanager.utils.Result
import org.jkondarewicz.kheperaivmanager.utils.TimeUtils
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ConflictCrossMessageListener(
    private val conflictCrossManager: ConflictCrossManager,
    mqttAsyncClient: IMqttAsyncClient,
    mqttTopicListeners: MqttTopicListeners
) : MqttTopicListener<ConflictTrackMessage>(mqttAsyncClient, mqttTopicListeners) {

    companion object {
        const val START_CONFLICT = "/conflicts/crossings/+/robots/+/+"
    }

    override fun onMessage(message: ConflictTrackMessage, vararg params: String) {
        if (params.size > 2) {
            when (params[2].toUpperCase()) {
                "START" -> conflictCrossManager.conflictStarted(message.deviceIdentifier, message.crossId, LocalDateTime.now())
                "STOP" -> conflictCrossManager.conflictStopped(message.deviceIdentifier, message.crossId)
            }
        }
    }

    override fun topics(): List<TopicWildcard> =
        listOf(TopicWildcard.of(START_CONFLICT))

    /**
     * Message structure: [device_identifier];[timestamp]:[cross_id]
     */
    override fun deserialize(incomeMessage: String): Result<ConflictTrackMessage> {
        val split = incomeMessage.split(ConflictTrackMessage.DELIMITER)
        if (split.size != ConflictTrackMessage.VALUES_QUANTITY) {
            return Result.failure()
        }
        val deviceIdentifier = split[0]
        val time = TimeUtils.fromTimeInMillis(split[1] as Long)
        val crossId = split[2] as Int
        return Result(ConflictTrackMessage(deviceIdentifier, time, crossId))
    }


}