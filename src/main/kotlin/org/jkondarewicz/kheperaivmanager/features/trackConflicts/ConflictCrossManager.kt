package org.jkondarewicz.kheperaivmanager.features.trackConflicts

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotId
import org.jkondarewicz.kheperaivmanager.features.trackConflicts.dto.ConflictVertex
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexId
import org.jkondarewicz.kheperaivmanager.mqtt.TopicWildcard
import org.jkondarewicz.kheperaivmanager.mqtt.senders.EmptyMessage
import org.jkondarewicz.kheperaivmanager.mqtt.senders.MqttMessageSender
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.concurrent.ConcurrentHashMap
import javax.annotation.PostConstruct
import kotlin.math.abs

typealias CrossId = Int

@Service
class ConflictCrossManager(
    private val conflictCrossStore: ConflictCrossStore,
    private val conflictCrossProperties: ConflictCrossProperties,
    private val mqttMessageSender: MqttMessageSender
) {

    private val log = LoggerFactory.getLogger(this::class.java)

    private val conflicts: ConcurrentHashMap<CrossId, MutableList<ConflictVertex>> = ConcurrentHashMap()

    fun conflictStarted(
        robotId: RobotId,
        crossId: CrossId,
        startConflictTime: LocalDateTime,
        sendStartMessageTime: LocalDateTime? = null
    ) {
        if (!conflicts.containsKey(crossId)) {
            conflicts[crossId] = mutableListOf()
        }
        val conflictVertex = ConflictVertex(crossId, robotId, startConflictTime, sendStartMessageTime, sendStartMessageTime != null)
        conflictCrossStore.save(conflictVertex.toEntity())
        conflicts[crossId]?.add(conflictVertex)
    }

    fun conflictStopped(robotId: RobotId, crossId: CrossId) {
        if (!conflicts.containsKey(crossId)) {
            conflicts[crossId] = mutableListOf()
        }
        conflictCrossStore.delete(robotId, crossId)
        conflicts[crossId]?.removeIf { it.robotId == robotId }
    }

    @Scheduled(fixedDelayString = "\${robots.scheduledTrackConflictCheckerInMilliseconds}")
    fun scheduledChecker() {
        log.info("Checking conflicted vertexes...")
        conflicts.forEach { entry ->
            clearRobotsThatProbablyMovedAway(entry)
            val anyRobotReceivedStartMessage = entry.value
                .map { checkSendStartMessage(it) }
                .any { it.sendStartMessage }
            if (!anyRobotReceivedStartMessage) {
                entry.value.firstOrNull()?.let {
                    log.info("Sending drive message to ${it.robotId}")
                    mqttMessageSender.sendMessage(
                        TopicWildcard.of("/robots/${it.robotId}/SHOULD_DRIVE"),
                        EmptyMessage()
                    )
                    it.sendStartMessageTime = LocalDateTime.now()
                    it.sendStartMessage = true
                    conflictCrossStore.save(it.toEntity())
                }
            }
        }
    }

    @PostConstruct
    fun initialized() {
        conflictCrossStore
            .getAll()
            .forEach {
                conflictStarted(
                    robotId = it.robotId,
                    crossId = it.crossId,
                    startConflictTime = it.startConflictTime,
                    sendStartMessageTime = it.sendStartMessageTime
                )
            }
    }

    private fun clearRobotsThatProbablyMovedAway(entry: Map.Entry<VertexId, MutableList<ConflictVertex>>) {
        while (true) {
            val min = entry.value.minByOrNull { it.startConflictTime.toEpochSecond(ZoneOffset.UTC) }
            val max = entry.value.maxByOrNull { it.startConflictTime.toEpochSecond(ZoneOffset.UTC) }
            if (min != null && max != null) {
                if (abs(Duration.between(max.startConflictTime, min.startConflictTime).seconds) > conflictCrossProperties.minimumDelayInSecondsBetweenStartConflictToDeleteRobotFromList) {
                    log.info("Removed ${min.robotId} from ${min.crossId} vertex")
                    conflictStopped(min.robotId, min.crossId)
                } else {
                    break
                }
            } else {
                break
            }
        }
    }

    private fun checkSendStartMessage(conflictVertex: ConflictVertex): ConflictVertex {
        if (conflictVertex.sendStartMessage && conflictVertex.sendStartMessageTime != null) {
            val duration = abs(Duration.between(LocalDateTime.now(), conflictVertex.sendStartMessageTime).seconds)
            if (duration > conflictCrossProperties.minimumSecondsToResendStartMessage) {
                return conflictVertex.apply {
                    this.sendStartMessage = false
                    this.sendStartMessageTime = null
                }
            }
        }
        return conflictVertex
    }

}