insert into tracks(name, preview_image, started) values('SAMPLE', null, false);
insert into vertexes("type", vertex_index, track_track_id, collect_point_quantity) values
    ('START', 0, 1, 0),
    ('NONE', 1, 1, 0),
    ('NONE', 2, 1, 0),
    ('NONE', 3, 1, 0),
    ('NONE', 4, 1, 0),
    ('COLLECT_POINT', 5, 1, 1),
    ('NONE', 6, 1, 0),
    ('NONE', 7, 1, 0),
    ('NONE', 8, 1, 0);
insert into edges(action, weight, from_vertex_id, to_vertex_id, cross_queue)
values
    ('FORWARD', 4, 1, 2, 1),
    ('FORWARD', 4, 2, 1, 1),
    ('FORWARD', 11, 2, 8, 1),
    ('FORWARD', 11, 8, 2, 1),
    ('FORWARD', 8, 1, 8, 1),
    ('FORWARD', 8, 8, 1, 1),
    ('FORWARD', 8, 2, 3, 1),
    ('FORWARD', 8, 3, 2, 1),
    ('FORWARD', 7, 3, 4, 1),
    ('FORWARD', 7, 4, 3, 1),
    ('FORWARD', 4, 3, 6, 1),
    ('FORWARD', 4, 6, 3, 1),
    ('FORWARD', 2, 3, 9, 1),
    ('FORWARD', 2, 9, 3, 1),
    ('FORWARD', 9, 4, 5, 1),
    ('FORWARD', 9, 5, 4, 1),
    ('FORWARD', 14, 4, 6, 1),
    ('FORWARD', 14, 6, 4, 1),
    ('FORWARD', 10, 5, 6, 1),
    ('FORWARD', 10, 6, 5, 1),
    ('FORWARD', 2, 6, 7, 1),
    ('FORWARD', 2, 7, 6, 1),
    ('FORWARD', 1, 7, 8, 1),
    ('FORWARD', 1, 8, 7, 1),
    ('FORWARD', 6, 7, 9, 1),
    ('FORWARD', 6, 9, 7, 1),
    ('FORWARD', 7, 8, 9, 1),
    ('FORWARD', 7, 9, 8, 1);

insert into tracks(name, preview_image, started) values('Trasa testowa', null, false);
insert into vertexes("type", vertex_index, track_track_id, collect_point_quantity) values
    ('START',           0, 2, 0),   --vertex_id = 10 #1
    ('NONE',            1, 2, 0),   --vertex_id = 11 #2
    ('NONE',            2, 2, 0),   --vertex_id = 12 #3
    ('COLLECT_POINT',   3, 2, 1),   --vertex_id = 13 #4
    ('NONE',            4, 2, 0),   --vertex_id = 14 #5
    ('NONE',            5, 2, 0),   --vertex_id = 15 #6
    ('COLLECT_POINT',   6, 2, 2),   --vertex_id = 16 #7
    ('NONE',            7, 2, 0),   --vertex_id = 17 #8
    ('NONE',            8, 2, 0),   --vertex_id = 18 #9
    ('NONE',            9, 2, 0),   --vertex_id = 19 #10
    ('NONE',            10, 2, 0),  --vertex_id = 20 #11
    ('COLLECT_POINT',   11, 2, 1),  --vertex_id = 21 #12
    ('NONE',            12, 2, 0),  --vertex_id = 22 #13
    ('NONE',            13, 2, 0),  --vertex_id = 23 #14
    ('NONE',            14, 2, 0);  --vertex_id = 24 #15
insert into edges(action, weight, from_vertex_id, to_vertex_id, cross_queue)
values
    ('FORWARD',      4, 10, 11, 1), --1, 2),
    ('FORWARD',      4, 11, 12, 2), --2, 3),
    ('TURN_RIGHT',  4, 12, 13, 3), --3, 4),
    ('TURN_RIGHT',  4, 13, 14, 3), --4, 5),
    ('FORWARD',      4, 12, 14, 3), --3, 5),
    ('TURN_LEFT',   4, 14, 15, 4), --5, 6),
    ('TURN_RIGHT',  4, 15, 16, 5), --6, 7),
    ('TURN_RIGHT',  4, 16, 17, 5), --7, 8),
    ('FORWARD',      4, 15, 17, 5), --6, 8),
    ('TURN_LEFT',   4, 17, 18, 6), --8, 9),
    ('TURN_LEFT',   4, 18, 19, 7), --9, 10),
    ('FORWARD',      4, 19, 20, 2), --10, 11),
    ('TURN_LEFT',   4, 20, 21, 8), --11, 12),
    ('TURN_LEFT',   4, 21, 22, 8), --12, 13),
    ('FORWARD',      4, 20, 22, 8), --11, 13),
    ('TURN_RIGHT',  4, 22, 23, 9), --13, 14),
    ('TURN_RIGHT',  4, 23, 24, 10), --14, 15),
    ('TURN_RIGHT',  4, 24, 11, 11), --15, 2),
    ('TURN_LEFT',   4, 24, 10, 11), --15, 1),
    ('TURN_RIGHT',  4, 11, 20,  2);