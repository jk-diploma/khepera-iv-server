INSERT INTO tracks(name, preview_image, started) VALUES('SAMPLE', null, false);
INSERT INTO vertexes(type, vertex_index, track_track_id, collect_point_quantity)
VALUES
    ('START', 0, 1, 0),
    ('NONE', 1, 1, 0),
    ('NONE', 2, 1, 0),
    ('NONE', 3, 1, 0),
    ('NONE', 4, 1, 0),
    ('COLLECT_POINT', 5, 1, 1),
    ('NONE', 6, 1, 0),
    ('NONE', 7, 1, 0),
    ('NONE', 8, 1, 0);
INSERT INTO edges( action, weight, from_vertex_id, to_vertex_id, cross_queue)
VALUES
    ('FORWARD', 4, 1, 2, 1),
    ('FORWARD', 4, 2, 1, 1),
    ('FORWARD', 11, 2, 8, 1),
    ('FORWARD', 11, 8, 2, 1),
    ('FORWARD', 8, 1, 8, 1),
    ('FORWARD', 8, 8, 1, 1),
    ('FORWARD', 8, 2, 3, 1),
    ('FORWARD', 8, 3, 2, 1),
    ('FORWARD', 7, 3, 4, 1),
    ('FORWARD', 7, 4, 3, 1),
    ('FORWARD', 4, 3, 6, 1),
    ('FORWARD', 4, 6, 3, 1),
    ('FORWARD', 2, 3, 9, 1),
    ('FORWARD', 2, 9, 3, 1),
    ('FORWARD', 9, 4, 5, 1),
    ('FORWARD', 9, 5, 4, 1),
    ('FORWARD', 14, 4, 6, 1),
    ('FORWARD', 14, 6, 4, 1),
    ('FORWARD', 10, 5, 6, 1),
    ('FORWARD', 10, 6, 5, 1),
    ('FORWARD', 2, 6, 7, 1),
    ('FORWARD', 2, 7, 6, 1),
    ('FORWARD', 1, 7, 8, 1),
    ('FORWARD', 1, 8, 7, 1),
    ('FORWARD', 6, 7, 9, 1),
    ('FORWARD', 6, 9, 7, 1),
    ('FORWARD', 7, 8, 9, 1),
    ('FORWARD', 7, 9, 8, 1);
INSERT INTO robots(robot_id, battery_percentage, heartbeat, which_in_order, assigned_track_track_id) VALUES
    ('ROBOT_1', 100, '2020-12-24 12:00:00', 1, 1),
    ('ROBOT_2',  75, '2020-12-24 08:00:00', null, null),
    ('ROBOT_3',  50, '2020-12-24 06:00:00', 2, 1);