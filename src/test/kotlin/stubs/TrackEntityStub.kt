package stubs

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackEntity

class TrackEntityStub {

    companion object {

        fun trackFor_SAMPLE() = TrackEntity(
            trackId = 1,
            previewImage = null,
            name = "Sample track for graph from site: https://www.geeksforgeeks.org/printing-paths-dijkstras-shortest-path-algorithm"
        ).also {
            it.addVertex(VertexEntityStub.vertex0_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex1_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex2_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex3_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex4_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex5_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex6_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex7_SAMPLE(it))
            it.addVertex(VertexEntityStub.vertex8_SAMPLE(it))
        }

    }

}