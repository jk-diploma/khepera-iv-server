package stubs

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.TrackEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

class VertexEntityStub {

    companion object {

        fun vertex0_SAMPLE(track: TrackEntity) = vertex0_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom0To1_SAMPLE(it, vertex1_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom0To7_SAMPLE(it, vertex7_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom1To0_SAMPLE(vertex1_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom7To0_SAMPLE(vertex7_SAMPLE, it))
        }

        fun vertex1_SAMPLE(track: TrackEntity) = vertex1_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom1To0_SAMPLE(it, vertex0_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom1To2_SAMPLE(it, vertex2_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom1To7_SAMPLE(it, vertex7_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom0To1_SAMPLE(vertex0_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom2To1_SAMPLE(vertex2_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom7To1_SAMPLE(vertex7_SAMPLE, it))
        }

        fun vertex2_SAMPLE(track: TrackEntity) = vertex2_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom2To1_SAMPLE(it, vertex1_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom2To3_SAMPLE(it, vertex3_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom2To5_SAMPLE(it, vertex5_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom2To8_SAMPLE(it, vertex8_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom1To2_SAMPLE(vertex1_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom3To2_SAMPLE(vertex3_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom5To2_SAMPLE(vertex5_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom8To2_SAMPLE(vertex8_SAMPLE, it))
        }

        fun vertex3_SAMPLE(track: TrackEntity) = vertex3_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom3To2_SAMPLE(it, vertex2_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom3To4_SAMPLE(it, vertex4_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom3To5_SAMPLE(it, vertex5_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom2To3_SAMPLE(vertex2_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom4To3_SAMPLE(vertex4_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom5To3_SAMPLE(vertex5_SAMPLE, it))
        }

        fun vertex4_SAMPLE(track: TrackEntity) = vertex4_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom4To3_SAMPLE(it, vertex3_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom4To5_SAMPLE(it, vertex5_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom3To4_SAMPLE(vertex3_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom5To4_SAMPLE(vertex5_SAMPLE, it))
        }

        fun vertex5_SAMPLE(track: TrackEntity) = vertex5_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom5To2_SAMPLE(it, vertex2_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom5To3_SAMPLE(it, vertex3_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom5To4_SAMPLE(it, vertex4_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom5To6_SAMPLE(it, vertex6_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom2To5_SAMPLE(vertex2_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom3To5_SAMPLE(vertex3_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom4To5_SAMPLE(vertex4_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom6To5_SAMPLE(vertex6_SAMPLE, it))
        }

        fun vertex6_SAMPLE(track: TrackEntity) = vertex6_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom6To5_SAMPLE(it, vertex5_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom6To7_SAMPLE(it, vertex7_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom6To8_SAMPLE(it, vertex8_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom5To6_SAMPLE(vertex5_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom7To6_SAMPLE(vertex7_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom8To6_SAMPLE(vertex8_SAMPLE, it))
        }

        fun vertex7_SAMPLE(track: TrackEntity) = vertex7_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom7To0_SAMPLE(it, vertex0_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom7To1_SAMPLE(it, vertex1_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom7To6_SAMPLE(it, vertex6_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom7To8_SAMPLE(it, vertex8_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom0To7_SAMPLE(vertex0_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom1To7_SAMPLE(vertex1_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom6To7_SAMPLE(vertex6_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom8To7_SAMPLE(vertex8_SAMPLE, it))
        }

        fun vertex8_SAMPLE(track: TrackEntity) = vertex8_SAMPLE.also {
            it.track = track
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom8To2_SAMPLE(it, vertex2_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom8To6_SAMPLE(it, vertex6_SAMPLE))
            it.addOutputEdge(EdgeEntityStub.edgeEntityFrom8To7_SAMPLE(it, vertex7_SAMPLE))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom2To1_SAMPLE(vertex2_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom6To8_SAMPLE(vertex6_SAMPLE, it))
            it.addInputEdge(EdgeEntityStub.edgeEntityFrom7To8_SAMPLE(vertex7_SAMPLE, it))
        }

        private val vertex0_SAMPLE = VertexEntity(
            vertexId = 1,
            type = VertexType.START,
            vertexIndex = 0
        )

        private val vertex1_SAMPLE = VertexEntity(
            vertexId = 2,
            type = VertexType.NONE,
            vertexIndex = 1
        )

        private val vertex2_SAMPLE = VertexEntity(
            vertexId = 3,
            type = VertexType.NONE,
            vertexIndex = 2
        )

        private val vertex3_SAMPLE = VertexEntity(
            vertexId = 4,
            type = VertexType.NONE,
            vertexIndex = 3
        )

        private val vertex4_SAMPLE = VertexEntity(
            vertexId = 5,
            type = VertexType.NONE,
            vertexIndex = 4
        )

        private val vertex5_SAMPLE = VertexEntity(
            vertexId = 6,
            type = VertexType.COLLECT_POINT,
            vertexIndex = 5,
            collectPointQuantity = 1
        )

        private val vertex6_SAMPLE = VertexEntity(
            vertexId = 7,
            type = VertexType.NONE,
            vertexIndex = 6
        )

        private val vertex7_SAMPLE = VertexEntity(
            vertexId = 8,
            type = VertexType.NONE,
            vertexIndex = 7
        )

        private val vertex8_SAMPLE = VertexEntity(
            vertexId = 9,
            type = VertexType.NONE,
            vertexIndex = 8
        )

    }

}