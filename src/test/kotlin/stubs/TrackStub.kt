package stubs

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.TrackEdge
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.TrackVertex
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.VertexIndex
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeAction
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

class TrackStub {

    companion object {

        fun tracksFor_SAMPLE() = listOf(
            Track(from = vertex(0, VertexType.START), to = vertex(1), summaryWeight = 4, path = listOf(edge(1, 4, 0, 1, fromType = VertexType.START))),
            Track(from = vertex(0, VertexType.START), to = vertex(2), summaryWeight = 12, path = listOf(edge(1, 4, 0, 1, fromType = VertexType.START), edge(7, 8, 1, 2))),
            Track(from = vertex(0, VertexType.START), to = vertex(3), summaryWeight = 19, path = listOf(edge(1, 4, 0, 1, fromType = VertexType.START), edge(7, 8, 1, 2), edge(9, 7, 2, 3))),
            Track(from = vertex(0, VertexType.START), to = vertex(4), summaryWeight = 21, path = listOf(edge(5, 8, 0, 7, fromType = VertexType.START), edge(24, 1, 7, 6), edge(22, 2, 6, 5, toType = VertexType.COLLECT_POINT), edge(20, 10, 5, 4, fromType = VertexType.COLLECT_POINT))),
            Track(from = vertex(0, VertexType.START), to = vertex(5, VertexType.COLLECT_POINT), summaryWeight = 11, path = listOf(edge(5, 8, 0, 7, fromType = VertexType.START), edge(24, 1, 7, 6), edge(22, 2, 6, 5, toType = VertexType.COLLECT_POINT))),
            Track(from = vertex(0, VertexType.START), to = vertex(6), summaryWeight = 9, path = listOf(edge(5, 8, 0, 7, fromType = VertexType.START), edge(24, 1, 7, 6))),
            Track(from = vertex(0, VertexType.START), to = vertex(7), summaryWeight = 8, path = listOf(edge(5, 8, 0, 7, fromType = VertexType.START))),
            Track(from = vertex(0, VertexType.START), to = vertex(8), summaryWeight = 14, path = listOf(edge(1, 4, 0, 1, fromType = VertexType.START), edge(7, 8, 1, 2), edge(13, 2, 2, 8)))
        )

        fun fullTrackToCollectFor_SAMPLE() = listOf(
            Track(from = vertex(0, VertexType.START), by = vertex(5, VertexType.COLLECT_POINT), to = vertex(0, VertexType.START), summaryWeight = 22, path = listOf(edge(5, 8, 0, 7, fromType = VertexType.START), edge(24, 1, 7, 6), edge(22, 2, 6, 5, toType = VertexType.COLLECT_POINT), edge(21, 2, 5, 6, fromType = VertexType.COLLECT_POINT), edge(23, 1, 6, 7), edge(6, 8, 7, 0, toType = VertexType.START)))
        )

        private fun vertex(vertexIndex: VertexIndex, vertexType: VertexType = VertexType.NONE) =
            TrackVertex((vertexIndex + 1), vertexIndex, vertexType, true)

        private fun edge(edgeId: Int, weight: Int, from: Int, to: Int, fromType: VertexType = VertexType.NONE, toType: VertexType = VertexType.NONE) =
            TrackEdge(
                edgeId = edgeId,
                edgeAction = EdgeAction.FORWARD,
                weight = weight,
                from = vertex(from, fromType),
                to = vertex(to, toType),
                crossId = 1
            )

    }

}