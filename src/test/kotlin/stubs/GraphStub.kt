package stubs

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Graph

class GraphStub {

    companion object {

        fun getGraph_SAMPLE() = Graph(
            vertexes = listOf(
                VertexStub.vertex0_SAMPLE(),
                VertexStub.vertex1_SAMPLE(),
                VertexStub.vertex2_SAMPLE(),
                VertexStub.vertex3_SAMPLE(),
                VertexStub.vertex4_SAMPLE(),
                VertexStub.vertex5_SAMPLE(),
                VertexStub.vertex6_SAMPLE(),
                VertexStub.vertex7_SAMPLE(),
                VertexStub.vertex8_SAMPLE()
            )
        )

        fun getAdjacencyMatrixForGraph_SAMPLE() = listOf(
            listOf(0, 4, 0, 0, 0, 0, 0, 8, 0),
            listOf(4, 0, 8, 0, 0, 0, 0, 11, 0),
            listOf(0, 8, 0, 7, 0, 4, 0, 0, 2),
            listOf(0, 0, 7, 0, 9, 14, 0, 0, 0),
            listOf(0, 0, 0, 9, 0, 10, 0, 0, 0),
            listOf(0, 0, 4, 14, 10, 0, 2, 0, 0),
            listOf(0, 0, 0, 0, 0, 2, 0, 1, 6),
            listOf(8, 11, 0, 0, 0, 0, 1, 0, 7),
            listOf(0, 0, 2, 0, 0, 0, 6, 7, 0)
        )

    }

}