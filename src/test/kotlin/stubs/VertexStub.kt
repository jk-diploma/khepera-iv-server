package stubs

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.NextVertex
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Vertex
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeAction
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexType

class VertexStub {

    companion object {

        fun vertex0_SAMPLE() = Vertex(
            vertexId = 1,
            vertexIndex = 0,
            vertexType = VertexType.START,
            nextVertexes = listOf(
                NextVertex(weight = 4, vertexIndex = 1, edgeId = 1, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 8, vertexIndex = 7, edgeId = 5, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex1_SAMPLE() = Vertex(
            vertexId = 2,
            vertexIndex = 1,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 4, vertexIndex = 0, edgeId = 2, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 8, vertexIndex = 2, edgeId = 7, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 11, vertexIndex = 7, edgeId = 3, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex2_SAMPLE() = Vertex(
            vertexId = 3,
            vertexIndex = 2,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 8, vertexIndex = 1, edgeId = 8, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 7, vertexIndex = 3, edgeId = 9, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 4, vertexIndex = 5, edgeId = 11, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 2, vertexIndex = 8, edgeId = 13, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex3_SAMPLE() = Vertex(
            vertexId = 4,
            vertexIndex = 3,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 7, vertexIndex = 2, edgeId = 10, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 9, vertexIndex = 4, edgeId = 15, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 14, vertexIndex = 5, edgeId = 17, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex4_SAMPLE() = Vertex(
            vertexId = 5,
            vertexIndex = 4,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 9, vertexIndex = 3, edgeId = 16, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 10, vertexIndex = 5, edgeId = 19, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex5_SAMPLE() = Vertex(
            vertexId = 6,
            vertexIndex = 5,
            vertexType = VertexType.COLLECT_POINT,
            nextVertexes = listOf(
                NextVertex(weight = 4, vertexIndex = 2, edgeId = 12, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 14, vertexIndex = 3, edgeId = 18, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 10, vertexIndex = 4, edgeId = 20, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 2, vertexIndex = 6, edgeId = 21, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 1
        )

        fun vertex6_SAMPLE() = Vertex(
            vertexId = 7,
            vertexIndex = 6,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 2, vertexIndex = 5, edgeId = 22, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 1, vertexIndex = 7, edgeId = 23, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 6, vertexIndex = 8, edgeId = 25, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex7_SAMPLE() = Vertex(
            vertexId = 8,
            vertexIndex = 7,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 8, vertexIndex = 0, edgeId = 6, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 11, vertexIndex = 1, edgeId = 4, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 1, vertexIndex = 6, edgeId = 24, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 7, vertexIndex = 8, edgeId = 27, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

        fun vertex8_SAMPLE() = Vertex(
            vertexId = 9,
            vertexIndex = 8,
            vertexType = VertexType.NONE,
            nextVertexes = listOf(
                NextVertex(weight = 2, vertexIndex = 2, edgeId = 14, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 6, vertexIndex = 6, edgeId = 26, edgeAction = EdgeAction.FORWARD, crossId = 1),
                NextVertex(weight = 7, vertexIndex = 7, edgeId = 28, edgeAction = EdgeAction.FORWARD, crossId = 1)
            ),
            conflictPossible = true,
            howMany = 0
        )

    }

}