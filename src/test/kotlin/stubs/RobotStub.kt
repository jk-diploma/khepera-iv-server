package stubs

import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotEntity
import java.time.LocalDateTime
import java.time.Month

class RobotStub {

    companion object {

        val robot1 = RobotEntity(
            robotId = "ROBOT_1",
            batteryPercentage = 100,
            heartbeat = LocalDateTime.of(2020, Month.DECEMBER, 24, 12, 0, 0),
            whichInOrder = 1,
            assignedTrack = TrackEntityStub.trackFor_SAMPLE()
        )

        val robot2 = RobotEntity(
            robotId = "ROBOT_2",
            batteryPercentage = 75,
            heartbeat = LocalDateTime.of(2020, Month.DECEMBER, 24, 8, 0, 0),
            whichInOrder = null,
            assignedTrack = null
        )

        val robot3 = RobotEntity(
            robotId = "ROBOT_3",
            batteryPercentage = 50,
            heartbeat = LocalDateTime.of(2020, Month.DECEMBER, 24, 6, 0, 0),
            whichInOrder = 2,
            assignedTrack = TrackEntityStub.trackFor_SAMPLE()
        )

    }

}