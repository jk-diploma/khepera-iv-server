package stubs

import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeAction
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.EdgeEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.entities.VertexEntity

class EdgeEntityStub {

    companion object {

        fun edgeEntityFrom0To1_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 1,
            weight = 4,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom1To0_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 2,
            weight = 4,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom1To7_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 3,
            weight = 11,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom7To1_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 4,
            weight = 11,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom0To7_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 5,
            weight = 8,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom7To0_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 6,
            weight = 8,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom1To2_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 7,
            weight = 8,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom2To1_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 8,
            weight = 8,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom2To3_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 9,
            weight = 7,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom3To2_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 10,
            weight = 7,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom2To5_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 11,
            weight = 4,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom5To2_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 12,
            weight = 4,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom2To8_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 13,
            weight = 2,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom8To2_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 14,
            weight = 2,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom3To4_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 15,
            weight = 9,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom4To3_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 16,
            weight = 9,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom3To5_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 17,
            weight = 14,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom5To3_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 18,
            weight = 14,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom4To5_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 19,
            weight = 10,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom5To4_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 20,
            weight = 10,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom5To6_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 21,
            weight = 2,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom6To5_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 22,
            weight = 2,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom6To7_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 23,
            weight = 1,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom7To6_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 24,
            weight = 1,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom6To8_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 25,
            weight = 6,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom8To6_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 26,
            weight = 6,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom7To8_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 27,
            weight = 7,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

        fun edgeEntityFrom8To7_SAMPLE(from: VertexEntity, to: VertexEntity) = EdgeEntity(
            edgeId = 28,
            weight = 7,
            action = EdgeAction.FORWARD,
            from = from,
            to = to,
            crossQueue = 1
        )

    }

}