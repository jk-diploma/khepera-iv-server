package utils

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Graph
import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Vertex

fun Graph.sorted() =
    Graph(vertexes = this.vertexes.map { Vertex(vertexId = it.vertexId, vertexIndex = it.vertexIndex, vertexType = it.vertexType, nextVertexes = it.nextVertexes.sortedBy { nv -> nv.vertexIndex }, conflictPossible = it.conflictPossible, howMany = it.howMany) }.sortedBy { it.vertexIndex })