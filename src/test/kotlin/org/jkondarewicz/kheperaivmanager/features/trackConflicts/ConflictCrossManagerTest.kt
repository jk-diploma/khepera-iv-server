package org.jkondarewicz.kheperaivmanager.features.trackConflicts

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import org.jkondarewicz.kheperaivmanager.mqtt.senders.MqttMessageSender
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.time.LocalDateTime

@ExtendWith(MockitoExtension::class)
class ConflictCrossManagerTest {

    @Mock
    private lateinit var conflictCrossStore: ConflictCrossStore

    @Mock
    private lateinit var mqttMessageSender: MqttMessageSender

    private val conflictCrossProperties: ConflictCrossProperties = ConflictCrossProperties(
        minimumSecondsToResendStartMessage = 10,
        minimumDelayInSecondsBetweenStartConflictToDeleteRobotFromList = 60
    )

    private lateinit var conflictCrossManager: ConflictCrossManager

    @BeforeEach
    fun initialize() {
        conflictCrossManager = ConflictCrossManager(conflictCrossStore, conflictCrossProperties, mqttMessageSender)
    }

    @Test
    fun `conflictStarted save conflict called`() {
        //Given
        val robotId = "hello"
        val vertexId = 1
        val startConflictTime = LocalDateTime.now()

        //When
        conflictCrossManager.conflictStarted(robotId, vertexId, startConflictTime)

        //Then
        verify(conflictCrossStore).save(any())
        verifyNoMoreInteractions(conflictCrossStore)
    }

    @Test
    fun `conflictStopped delete conflict called`() {
        //Given
        val robotId = "hello"
        val vertexId = 1

        //When
        conflictCrossManager.conflictStopped(robotId, vertexId)

        //Then
        verify(conflictCrossStore).delete(robotId, vertexId)
        verifyNoMoreInteractions(conflictCrossStore)
    }

    @Test
    fun `scheduledChecker check if robot was tagged as not sent after delay`() {
        //Given
        val now = LocalDateTime.now()
        val startConflictTime = now.minusSeconds(50)
        val sentStartMessage = now.minusSeconds(15)

        //When
        conflictCrossManager.conflictStarted("ROBOT", 1, startConflictTime, sentStartMessage)
        conflictCrossManager.conflictStarted("ROBOT2", 1, startConflictTime)
        conflictCrossManager.scheduledChecker()

        //Then
        verify(conflictCrossStore, times(3)).save(any())
        verifyNoMoreInteractions(conflictCrossStore)
    }

    @Test
    fun `scheduledChecker check if any robot does not changed message sent state`() {
        //Given
        val now = LocalDateTime.now()
        val startConflictTime = now.minusSeconds(50)
        val sentStartMessage = now.minusSeconds(5)

        //When
        conflictCrossManager.conflictStarted("ROBOT", 1, startConflictTime, sentStartMessage)
        conflictCrossManager.conflictStarted("ROBOT2", 1, startConflictTime)
        conflictCrossManager.scheduledChecker()

        //Then
        verify(conflictCrossStore, times(2)).save(any())
        verifyNoMoreInteractions(conflictCrossStore)
    }

    @Test
    fun `scheduledChecker when none robot received start message, sent it`() {
        //Given
        val now = LocalDateTime.now()
        val startConflictTime = now.minusSeconds(50)

        //When
        conflictCrossManager.conflictStarted("ROBOT", 1, startConflictTime)
        conflictCrossManager.conflictStarted("ROBOT2", 1, startConflictTime)
        conflictCrossManager.scheduledChecker()

        //Then
        verify(conflictCrossStore, times(3)).save(any())
        verifyNoMoreInteractions(conflictCrossStore)
    }

    @Test
    fun `scheduledChecker check if removed robot after too large delay`() {
        //Given
        val now = LocalDateTime.now()
        val startConflictTime = now.minusSeconds(10)
        val robotToDelete = "ROBOT2"
        val vertexId = 1
        val startConflictTimeOfRobotToDelete = now.minusSeconds(80)
        val sentStartMessage = now.minusSeconds(5)

        //When
        conflictCrossManager.conflictStarted("ROBOT", vertexId, startConflictTime, sentStartMessage)
        conflictCrossManager.conflictStarted(robotToDelete, vertexId, startConflictTimeOfRobotToDelete)
        conflictCrossManager.scheduledChecker()

        //Then
        verify(conflictCrossStore, times(2)).save(any())
        verify(conflictCrossStore).delete(robotToDelete, vertexId)
        verifyNoMoreInteractions(conflictCrossStore)
    }

    @Test
    fun `scheduledChecker check if robot was removed when had sent message state set and send start message to another one robot`() {
        //Given
        val now = LocalDateTime.now()
        val startConflictTime = now.minusSeconds(10)
        val robotToDelete = "ROBOT2"
        val vertexId = 1
        val startConflictTimeOfRobotToDelete = now.minusSeconds(80)

        //When
        conflictCrossManager.conflictStarted("ROBOT", vertexId, startConflictTime)
        conflictCrossManager.conflictStarted(robotToDelete, vertexId, startConflictTimeOfRobotToDelete)
        conflictCrossManager.scheduledChecker()

        //Then
        verify(conflictCrossStore, times(3)).save(any())
        verify(conflictCrossStore).delete(robotToDelete, vertexId)
        verifyNoMoreInteractions(conflictCrossStore)
    }

}