package org.jkondarewicz.kheperaivmanager.features.robots.store

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.jkondarewicz.kheperaivmanager.features.robots.store.dto.RobotInfo
import org.jkondarewicz.kheperaivmanager.features.robots.store.entities.RobotEntity
import org.jkondarewicz.kheperaivmanager.features.tracks.TrackRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import stubs.RobotStub
import stubs.TrackEntityStub
import java.time.LocalDateTime
import java.util.*

@ExtendWith(MockitoExtension::class)
class RobotsStoreServiceTest {

    @Mock
    private lateinit var trackRepository: TrackRepository

    @Mock
    private lateinit var robotsRepository: RobotsRepository

    private lateinit var robotsStoreService: RobotsStoreService

    @BeforeEach
    fun initialize() {
        robotsStoreService = RobotsStoreService(robotsRepository, trackRepository)
    }

    @Test
    fun `heartbeat of robot`() {
        //Given
        val expectedRobotEntity = RobotInfo(
            robotId = "robot_test",
            batteryPercentage = 100,
            lastHeartbeat = LocalDateTime.now()
        )
        whenever(robotsRepository.findById(expectedRobotEntity.robotId))
            .thenReturn(Optional.empty())
        whenever(robotsRepository.save(any()))
            .thenReturn(RobotEntity.of(expectedRobotEntity.robotId, expectedRobotEntity.batteryPercentage, expectedRobotEntity.lastHeartbeat))


        //When
        val robot = robotsStoreService.heartbeat(expectedRobotEntity.robotId, expectedRobotEntity.batteryPercentage, expectedRobotEntity.lastHeartbeat)

        //Then
        assertEquals(expectedRobotEntity, robot)
    }

    @Test
    fun `heartbeat, of non existing robot`() {
        //Given
        val existingRobot = RobotStub.robot1
        val robotId = existingRobot.robotId
        val batteryPercentage = 70
        val heartbeat = LocalDateTime.now()
        whenever(robotsRepository.findById(existingRobot.robotId))
            .thenReturn(Optional.of(existingRobot))
        whenever(robotsRepository.save(any()))
            .thenReturn(existingRobot.also {
                it.heartbeat = heartbeat
                it.batteryPercentage = batteryPercentage
            })
        val expectedRobot = RobotInfo.ofRobotEntity(existingRobot).copy(batteryPercentage = batteryPercentage, lastHeartbeat = heartbeat)

        //When
        val robot = robotsStoreService.heartbeat(robotId, batteryPercentage, heartbeat)

        //Then
        assertEquals(expectedRobot, robot)
    }

    @Test
    fun `findAll, should return list of robots sorted`() {
        //Given
        val robotsEntity = listOf(RobotStub.robot1, RobotStub.robot2, RobotStub.robot3)
        val expectedRobots = listOf(RobotStub.robot2, RobotStub.robot1, RobotStub.robot3).map { RobotInfo.ofRobotEntity(it) }
        whenever(robotsRepository.findAll())
            .thenReturn(robotsEntity)

        //When
        val robots = robotsStoreService.robotsInfo()

        //Then
        assertEquals(expectedRobots, robots)
    }

    @Test
    fun `findAll, should return empty list`() {
        //Given
        val robotsEntity = listOf<RobotEntity>()
        val expectedRobots = listOf<RobotInfo>()
        whenever(robotsRepository.findAll())
            .thenReturn(robotsEntity)

        //When
        val robots = robotsStoreService.robotsInfo()

        //Then
        assertEquals(expectedRobots, robots)
    }

    @Test
    fun `assignTrack, should throw exception when track not found`() {
        //Given
        val nonExistingTrackId = -1
        val anyRobotId = "any"
        whenever(trackRepository.findById(nonExistingTrackId))
            .thenReturn(Optional.empty())

        //When
        val exception = Assertions.assertThrows(Exception::class.java) {
            robotsStoreService.assignTrack(anyRobotId, nonExistingTrackId)
        }

        //Then
        assertEquals("Track with id $nonExistingTrackId does not exist", exception.message)
    }

    @Test
    fun `assignTrack, should throw exception when robot not found`() {
        //Given
        val track = TrackEntityStub.trackFor_SAMPLE()
        val trackId = track.getIdOrException()
        val nonExistingRobotId = "non_exists"
        whenever(trackRepository.findById(trackId))
            .thenReturn(Optional.of(track))
        whenever(robotsRepository.findById(nonExistingRobotId))
            .thenReturn(Optional.empty())

        //When
        val exception = Assertions.assertThrows(Exception::class.java) {
            robotsStoreService.assignTrack(nonExistingRobotId, trackId)
        }

        //Then
        assertEquals("Robot with id $nonExistingRobotId does not exist", exception.message)
    }

    @Test
    fun `unassignTrack, should throw exception when robot not found`() {
        //Given
        val trackId = -1
        val nonExistingRobotId = "non_exists"
        whenever(robotsRepository.findById(nonExistingRobotId))
            .thenReturn(Optional.empty())

        //When
        val exception = Assertions.assertThrows(Exception::class.java) {
            robotsStoreService.unassignTrack(nonExistingRobotId, trackId)
        }

        //Then
        assertEquals("Robot with id $nonExistingRobotId does not exist", exception.message)
    }

}