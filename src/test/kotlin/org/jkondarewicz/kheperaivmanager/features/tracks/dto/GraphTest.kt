package org.jkondarewicz.kheperaivmanager.features.tracks.dto

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import stubs.GraphStub

class GraphTest {

    @Test
    fun `convert graph to adjacency matrix`() {
        //Given
        val graph = GraphStub.getGraph_SAMPLE()
        val expectedAdjacencyMatrix = GraphStub.getAdjacencyMatrixForGraph_SAMPLE()

        //When
        val adjacencyMatrix = graph.toAdjacencyMatrix()

        //Then
        assertEquals(expectedAdjacencyMatrix, adjacencyMatrix)
    }

}