package org.jkondarewicz.kheperaivmanager.features.tracks

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Track
import org.jkondarewicz.kheperaivmanager.features.tracks.shortestPath.DijkstraShortestPathService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import stubs.GraphStub
import stubs.TrackStub

class DijkstraShortestPathServiceTest {

    @Test
    fun `generate tracks from vertex 0`() {
        //Given
        val service = DijkstraShortestPathService()
        val graph = GraphStub.getGraph_SAMPLE()
        val sourceIndex = 0
        val expectedTracks = TrackStub.tracksFor_SAMPLE()

        //When
        val tracks = service.generateShortestPaths(graph, sourceIndex)

        //Then
        tracks.assertEquals(expectedTracks)
    }

    @Test
    fun `generate track from vertex 0 to vertex 6`() {
        //Given
        val service = DijkstraShortestPathService()
        val graph = GraphStub.getGraph_SAMPLE()
        val sourceIndex = 0
        val targetIndex = 6
        val expectedTrack = TrackStub.tracksFor_SAMPLE().first { track -> track.to.vertexIndex == targetIndex }

        //When
        val track = service.generateShortestPath(graph, sourceIndex, targetIndex)

        //Then
        track.assertEquals(expectedTrack)
    }

    private fun List<Track>.assertEquals(tracks: List<Track>) =
        this.forEachIndexed { index, track ->
            track.assertEquals(tracks[index], index)
        }

    private fun Track.assertEquals(expected: Track, index: Int = 0) {
        assertEquals(expected.from, from, "From incorrect at index $index")
        assertEquals(expected.to, to, "To incorrect at index $index")
        assertEquals(expected.summaryWeight, summaryWeight, "Summary weight incorrect at index $index")
        assertEquals(expected.path, path, "Path incorrect at index $index")
    }

}