package org.jkondarewicz.kheperaivmanager.features.tracks.entities

import org.jkondarewicz.kheperaivmanager.features.tracks.dto.Graph
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import stubs.GraphStub
import stubs.TrackEntityStub
import utils.sorted


class TrackEntityTest {

    @Test
    fun `test converting track entity into graph`() {
        //Given
        val track = TrackEntityStub.trackFor_SAMPLE()
        val expectedGraph = GraphStub.getGraph_SAMPLE()

        //When
        val graph = track.toGraph().sorted()

        //Then
        graph.assertEquals(expectedGraph)
    }

    @Test
    fun `test converting track entity into adjacency matrix`() {
        //Given
        val track = TrackEntityStub.trackFor_SAMPLE()
        val expectedAdjacencyMatrix = GraphStub.getGraph_SAMPLE().toAdjacencyMatrix()

        //When
        val adjacencyMatrix = track.toGraph().toAdjacencyMatrix()

        //Then
        assertEquals(expectedAdjacencyMatrix, adjacencyMatrix)
    }

    private fun Graph.assertEquals(expected: Graph) {
        this.vertexes.forEachIndexed { index, vertex ->
            assertEquals(vertex, expected.vertexes[index])
        }
    }

}