package org.jkondarewicz.kheperaivmanager.features.tracks

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import stubs.GraphStub
import stubs.TrackStub
import utils.sorted

@SpringBootTest
class TracksServiceIntegrationTest {

    @Autowired
    private lateinit var trackRepository: TrackRepository

    @Autowired
    private lateinit var tracksService: TracksService

    @Test
    @Transactional
    fun `check if track exists`() {
        //Given
        val trackId = 1
        val expectedGraph = GraphStub.getGraph_SAMPLE().sorted()

        //When
        val track = trackRepository.findByIdOrNull(trackId)?.toGraph()?.sorted() ?: error("Track not found")

        //Then
        assertEquals(expectedGraph, track)
    }

    @Test
    fun `check generated track with collect points`() {
        //Given
        val trackId = 1
        val expectedTrack = TrackStub.fullTrackToCollectFor_SAMPLE()

        //When
        val trackWithCollectPointsIncluded = tracksService.findShortestPathsToCollectPointAndBack(trackId)

        //Then
        assertEquals(expectedTrack, trackWithCollectPointsIncluded)
    }

}