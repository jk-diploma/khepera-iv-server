package org.jkondarewicz.kheperaivmanager.mqtt

import org.junit.jupiter.api.Assertions.assertDoesNotThrow
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.function.Consumer

class TopicWildcardTest {

    @Test
    fun `create new instance of TopicWildcard, happy path`() {
        //Given
        val validPaths = listOf(
            "/hello",
            "/hello/janek",
            "/1-server",
            "/temperature/+",
            "/temperature/+/#"
        )

        //When
        val getFromPath =  Consumer <String> { topic -> TopicWildcard.of(topic) }

        //Then
        validPaths.forEach { assertDoesNotThrow { getFromPath.accept(it) } }
    }

    @Test
    fun `create new instance of TopicWildcard, throws TopicNotValidException`() {
        //Given
        val validPaths = listOf(
            "/hell%o/",
            "hell\$o",
            "1he@llo/",
            "/h!ello/+",
            "/hąello+",
            "# /hello/+",
            "hello",
            "123",
            "/hello/#/+",
            "/hello/++",
            "/hello/+/##"
        )

        //When
        val getFromPath =  Consumer <String> { topic -> TopicWildcard.of(topic) }

        //Then
        validPaths.forEach { assertThrows <TopicNotValidException> { getFromPath.accept(it) } }
    }

}