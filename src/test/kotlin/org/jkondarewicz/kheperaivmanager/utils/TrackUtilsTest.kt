package org.jkondarewicz.kheperaivmanager.utils

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import stubs.TrackStub

@ExtendWith(MockitoExtension::class)
class RobotsStoreServiceTest {

    @Test
    fun `assignTracksToRobots more robots than tracks`() {
        //Given
        val track = TrackStub.fullTrackToCollectFor_SAMPLE().first()
        val tracks = listOf(track, track, track)
        val robotIds = listOf(1, 2, 3, 4).map { it.toString() }

        //When
        val tracksAssigned = tracks.assignTracksToRobots(robotIds)

        //Then
        assertEquals(tracksAssigned["1"], listOf(track))
        assertEquals(tracksAssigned["2"], listOf(track))
        assertEquals(tracksAssigned["3"], listOf(track))
        assertFalse(tracksAssigned.containsKey("4"))
    }

    @Test
    fun `assignTracksToRobots less robots than tracks`() {
        //Given
        val track = TrackStub.fullTrackToCollectFor_SAMPLE().first()
        val tracks = listOf(track, track, track, track, track)
        val robotIds = listOf(1, 2).map { it.toString() }

        //When
        val tracksAssigned = tracks.assignTracksToRobots(robotIds)

        //Then
        assertEquals(tracksAssigned["1"], listOf(track, track, track))
        assertEquals(tracksAssigned["2"], listOf(track, track))
    }

    @Test
    fun `toMessage more robots than tracks`() {
        //Given
        val track = TrackStub.fullTrackToCollectFor_SAMPLE().first()
        val tracks = listOf(track, track, track)
        val robotIds = listOf(1, 2, 3, 4).map { it.toString() }
        val trackAsMessage = "1:F:N-1:F:N-1:F:C-1:F:N-1:F:N-1:F:S"

        //When
        val tracksAssigned = tracks.assignTracksToRobots(robotIds)
        val firstRobot = tracksAssigned.toMessage("1")
        val secondRobot = tracksAssigned.toMessage("2")
        val thirdRobot = tracksAssigned.toMessage("3")
        val fourthRobot = tracksAssigned.toMessage("4")

        //Then
        assertEquals(trackAsMessage, firstRobot)
        assertEquals(trackAsMessage, secondRobot)
        assertEquals(trackAsMessage, thirdRobot)
        assertNull(fourthRobot)
    }

    @Test
    fun `toMessage less robots than tracks`() {
        //Given
        val track = TrackStub.fullTrackToCollectFor_SAMPLE().first()
        val tracks = listOf(track, track, track, track, track)
        val robotIds = listOf("1", "2")
        val trackAsMessage = "1:F:N-1:F:N-1:F:C-1:F:N-1:F:N-1:F:S"

        //When
        val tracksAssigned = tracks.assignTracksToRobots(robotIds)
        val firstRobot = tracksAssigned.toMessage("1")
        val secondRobot = tracksAssigned.toMessage("2")

        //Then
        assertEquals(listOf(trackAsMessage, trackAsMessage, trackAsMessage).joinToString(separator = "-"), firstRobot)
        assertEquals(listOf(trackAsMessage, trackAsMessage).joinToString(separator = "-"), secondRobot)
    }

}