# Khepera IV server

# 1. Description
This is server side of Khepera IV application manager. 
It is used to hold defined by user tracks (currently every track is hardcoded in database),
assigning / removing robots to / from track and to start / stop track (send command to all robots
assigned to track to start moving to collect goods).
# 2. Architecture
Server application make use of two protocols: HTTP (REST) and MQTT (TCP / IP). 
* Protocol HTTP is used to create GUI (Graphical User Interface) to users. GUI has a few functions: 
assigning robots to tracks, removing robots from tracks, starting / stopping tracks and viewing 
robots heartbeats.  
* Protocol MQTT is used as communication protocol between robots and server. 
For example on GUI user can assign robot to track and on assign event server sends message to 
properly robots to start moving
# 3. Packaging
Application has strict package structure. Packages with properly name have precise purpose:
* `configuration` - it contains all configuration classes of application. Because it is rather small application
so it only has MQTT connection configuration
* `features` - this package contains all application features. So inside this package there are next ones.
Every package inside should have properly name telling what feature it is inside. For example package: 
`features.robots.heartbeat` contains all classes that are used to listen and react on Heartbeat messages from robots.
* `mqtt` - this package is special. It contains all business logic related to establish connection with
MQTT server and has base listener that can be used to listen on messages by extending it. 
Also it has sending messages wrapper.
* `utils` - there are utils.
# 4. Running application
Running application is really easy. It could be done two ways:
1. Container way:
    1. Install `docker` and `docker-compose`
    1. Go to root directory and run in terminal `docker-compose up --build -d`
    1. Done
1. Application installation way:
    1. Install `postgresql` and `mosquitto`
    1. Done

Either You chose first or second way You should check `src/main/resources/application.yaml` properties 
and set mqtt and database host settings. After that You should run command `./gradlew bootJar` 
which will run application.

After that You can launch browser and go to: `http://localhost:8080/tracks` (host and port can be different if You
change application properties!).
